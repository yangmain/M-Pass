#  更新记录
> 2019年10月1号~31号
## common
### common-base 模块
1. 2019年10月8日  
    ``基础模块划分整理common基础公共模块;framework-discovery插件工厂模块、系统部署模块;系统SPI拓展开发Banner、config、logback``
2. 2019年10月9日  
    ``component组件完善banner、config、logback、redis、spring;common 常量管理，业务返回实体以及多语言处理;resources 系统资源管理``
3. 2019年10月10日
    ``国际化语言,常用工具类(cookie,json,reflect,trace..)，加密方式(aes,des,md5,rsa..)，系统错误基类(KmssException,KmssServiceException,ParamsNotValidException...)，系统拦截器(CompositeFilterProxy,CorsSecurityFilter,RsaPublicKeyFilter...)``  
